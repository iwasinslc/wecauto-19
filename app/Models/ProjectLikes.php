<?php
namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Projects
 * @package App\Models
 *
 * @property Projects project_id
 * @property integer is_like
 * @property User user_id
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class ProjectLikes extends Model
{
    use ModelTrait;

    /** @var string $table */
    public $table = 'project_likes';

    use Uuids;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var array $timestamps */
    public $timestamps = ['created_at', 'updated_at'];

    /** @var array $fillable */
    protected $fillable = [
        'project_id',
        'is_like',
        'user_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
