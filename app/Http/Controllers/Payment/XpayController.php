<?php
namespace App\Http\Controllers\Payment;

use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Modules\PaymentSystems\XpayModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class XpayController
 * @package App\Http\Controllers\Payment
 */
class XpayController extends PaymentController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function topUp()
    {
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = session('topup.payment_system');

        /** @var Currency $currency */
        $currency = session('topup.currency');

        if (empty($paymentSystem) || empty($currency)) {
            return redirect()->route('profile.topup')->with('error', __('Can not process your request, try again.'));
        }

        $amount = abs(session('topup.amount'));
        $user          = Auth::user();
        $wallet        = session('topup.wallet');

        if (empty($wallet)) {
            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
        }

        /** @var Transaction $transaction */
        $transaction = Transaction::enter($wallet, $amount, $currency, $paymentSystem);
        $comment     = config('money.xpay_memo');
        $merchantId  = config('money.xpay_merchant_id');

        if (null !== $transaction) {
            $transaction->result = substr(md5($transaction->id), 0, 20);
            $transaction->save();
        }

        User::notifyAdminsViaNotificationBot('topup_request', [
            'transaction' => $transaction,
        ]);

        return view('ps.xpay', [
            'currency'      => $currency,
            'user'          => $user,
            'wallet'        => $wallet,
            'transaction'   => $transaction,
            'paymentSystem' => $paymentSystem,
            'comment'       => $comment,
            'merchantId'    => $merchantId,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function status(Request $request)
    {
        if (!isset($request->amount)
            || !isset($request->datetime)
            || !isset($request->payment_id)
            || !isset($request->merchant)
            || !isset($request->order_id)
            || !isset($request->payment_status)
            || !isset($request->sign)) {
            \Log::info('Xpay. Strange request from: '.$request->ip().'. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        $paymentSystem = PaymentSystem::where('code', 'xpay')->first();
        $currency      = Currency::where('code', 'RUR')->first();
        $transaction   = Transaction::where('result', $request->order_id)
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->first();

        if (null == $transaction) {
            \Log::info('Xpay. Transaction not found. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        $secretkey = config('money.xpay_api_secret_word');
        $hash      = md5($secretkey.':'.number_format($transaction->amount, 2, '.', '').':'.$request->payment_id.':'.$request->order_id.':'.$request->merchant);

        if ($hash != $request->sign) {
            \Log::info('Xpay. Hash is not correct: '.$request->ip().'. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        if ($transaction->result == 'success') {
            \Log::info('Xpay. Transaction already completed. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        /** @var Wallet $wallet */
        $wallet        = $transaction->user
            ->wallets()
            ->where('currency_id', $currency->id)
            ->first();

        if (null === $wallet) {
            $wallet = Wallet::newWallet($transaction->user, $currency, $paymentSystem);
        }

        \Log::info('Found wallet for topup: '.print_r($wallet,true));

        if (null !== $wallet) {
            $transaction->batch_id = $request->payment_id;
            $transaction->result = 'complete';
            $transaction->payment_system_id = $paymentSystem->id;
            $transaction->currency_id = $currency->id;
            $transaction->save();

            $commission = $transaction->amount * 0.01 * $transaction->commission;
            $wallet->refill(($transaction->amount), '');
            $transaction->update(['approved' => true]);
            $this->autocreateDeposit($transaction);
            (new XpayModule())->getBalances(); // обновляем баланс нашего внешнего кошелька в БД
            return response('ok');
        }

        return response('error wallet', 500);
    }
}
