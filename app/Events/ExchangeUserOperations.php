<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExchangeUserOperations implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $broadcastQueue = 'default';

    public $user;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {

        $this->data = [
            'user_id'=>$user->id,
            'sell_limit'=>$user->sellLimit(),
            'buy_limit'=>$user->buyLimit()
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('exchange.user.'. $this->data['user_id']);
    }
}
