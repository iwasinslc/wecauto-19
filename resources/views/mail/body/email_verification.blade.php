@extends('mail.layouts.main')
@section('content')
<p>{{ __('Verify your email') }}:</p>
<div>
    Click <a href="{{route('email.confirm', ['hash' => $email_verification_hash])}}"><b>HERE</b></a> to confirm your password
</div><br/>
<div style="word-break: break-all">
    {{ __('Or open confirmation link in your browser') }}: {{ route('email.confirm', ['hash' => $email_verification_hash]) }}
</div>
@endsection