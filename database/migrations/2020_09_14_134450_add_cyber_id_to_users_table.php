<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCyberIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'cyber_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('cyber_id')->nullable(true)->unique();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'cyber_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('cyber_id');
            });
        }
    }
}
